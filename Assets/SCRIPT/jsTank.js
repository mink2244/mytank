﻿#pragma strict

var speed = 5;				// 탱크의 이동 속도(m/s)
var rotSpeed = 12;			// 탱크의 회전 속도(각도/s)
var turret : GameObject;	// 포탑

var power = 600;			// 포탄 발사 속도
var bullet : Transform;		// 포탄

var explosion : Transform;	// 포구 폭발 파티클
var snd : AudioClip;		// 포구 폭음
function Start () {

}

function Update () {
	var amtToMove = speed * Time.deltaTime;		// 프레임에 탱크가 이동할 거리
	var amtToRot = rotSpeed * Time.deltaTime;	// 프레임에 탱크가 회전할 각도
	
	var front = Input.GetAxis("Vertical");		// 탱크 전후진(수직), 벡터
	var ang = Input.GetAxis("Horizontal");		// 탱크 좌우(수평) 회전 방향, 벡터
	var ang2 = Input.GetAxis("MyTank");			// 포탑 회전방향, 벡터
	
	transform.Translate(Vector3.forward * front * amtToMove);	// 탱크 전후진
	transform.Rotate(Vector3(0, ang * amtToRot, 0));			// 탱크 회전
	turret.transform.Rotate(Vector3.up * ang2 * amtToRot);		// 포탑 회전
	
	// 포탄 발사
	if (Input.GetButtonDown("Fire1")) {
		var spPoint = GameObject.Find("spawnPoint");	// spawnPoint 정보 읽기
		
		Instantiate(explosion, spPoint.transform.position, Quaternion.identity);	// 포구 앞의 화염
		AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);		// 포구 오디오 소스
		
		var myBullet = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
	}
}