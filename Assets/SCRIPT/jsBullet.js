﻿#pragma strict

var snd : AudioClip;		// 사운드 파일
var explosion : Transform;	// 폭발

function Start () {

}

function Update () {

}

function OnTriggerEnter(coll : Collider) {
	// 폭파 불꽃 표시, 포탄이 명중하면 폭파 불꽃을 표시하고 사운드를 출력한다.
	Instantiate(explosion, transform.position, Quaternion.identity);
	AudioSource.PlayClipAtPoint(snd, transform.position);
	Destroy(gameObject);
	
	if(coll.gameObject.tag == "WALL") {		// 충돌체의 태그가 WALL인지 식별
		//Instantiate(explosion, coll.transform.position, Quaternion.identity);	// 폭발 
		//AudioSource.PlayClipAtPoint(snd, transform.position);
		Destroy(coll.gameObject);
		//Destroy(gameObject);
	} else if (coll.gameObject.tag == "ENEMY") {
		jsScore.hit++;					// 점수 증가
		if(jsScore.hit > 5) {			// 5점 이상이면
			Destroy(coll.transform.root.gameObject);	// 적군 탱크 파괴
			// 승리 화면으로 분기
			Application.LoadLevel("WinGame");
		}
	} else if (coll.gameObject.tag == "TANK") {
		jsScore.lose++;
		if (jsScore.lose > 5) {			// 실점 증가
			// 패배 화면으로 분기			// 5점 이상이면
			Application.LoadLevel("LostGame");
		}
	}
	
	AudioSource.PlayClipAtPoint(snd, transform.position);		// 사운드 출력, transform->포탄자신.
	Destroy(gameObject);		// 포탄 제거
}