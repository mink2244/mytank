﻿#pragma strict

private var power = 1200;		// 적군 폭탄 발사속도

var bullet : Transform;		// 적군 포탄
var target : Transform;		// LookAt() 목표
var spPoint : Transform;	// 적군의 spPoint
var explosion : Transform;	// 포구 앞의 화염
var snd : AudioClip;		// 발사음

private var ftime : float = 0.0;		// 적군 탱크의 사격 제한 시간

function Start () {

}

function Update () {
	transform.LookAt(target);	// 아군 방향으로 회전
	ftime += Time.deltaTime;	// 경과 시간 누적
	
	var hit : RaycastHit;		// 탐색 결과 저장
	//var fwd = Vector3.forward;	// 포탑의 전방, 로컬 좌표
	var fwd = transform.TransformDirection(Vector3.forward);	// 글로벌 좌표
	
	// 탐색 실패
	if (Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
	if (hit.collider.gameObject.tag != "TANK" || ftime < 2) return;		// 시간 체크
	
	if (hit.collider.gameObject.tag != "TANK") return;	// 아군 탱크가 아니면 발포하지 않음
	
	// 포구 앞의 화염
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	
	// 포탄
	var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
	obj.rigidbody.AddForce(fwd * power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);	// 포탄 발사음
	
	Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green);	// 거리 표지
	if (Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
	Debug.Log(hit.collider.gameObject.name);	// 참색한 오브젝트 이름 표시
	
	ftime = 0;	// 경과 시간 리셋
}