﻿#pragma strict

static var hit : int = 0;			// 고정된 Hit 객체를 생성하고 0을 저장하라.
static var lose : int = 0;		// 고정된 lose 객체를 생성하고 0을 저장하라.


function Start() {	// 게임을 시작할 때마다 다음 함수를 정의하여라.
	hit = 0; //hit에 0을 저장하여라.
	lose = 0; //lose에 0을 저장하여라.
}

function OnGUI() {			// onGUI 함수를 다음과 같이 정의하여라.
	var w = Screen.width;   // 화면의 폭 값을 변수 w에 생성하고 저장하라
	                 
	TankEnergy(lose);  // 함수 TankEnergy(lose)를 불러와라.
	EnemyEnergy(hit);  // 함수 EnemyEnergy(hit)를 불러와라.
	
	GUI.Label(Rect(40, 25, 150, 30), "탱크 에너지" ); // GUI 객체의 Label 속성을 (40, 25)에 (150, 30)의 크기의 '명중 :'텍스트와 hit에 저장된 정수를 출력하도록 설정하라. 
	GUI.Label(Rect(w-200, 25, 150, 30), "적 탱크 에너지" );// GUI 객체의 Label 속성을 (w-200, 25)에 (150, 30)의 크기의 '피격 :'텍스트와 lose에 저장된 정수를 출력하도록 설정하라.
}

	
function TankEnergy(lose : int) { //DrawProgressBar2의 속성이 int 형태의 객체인 함수을 다음과 같이 정의 하여라
    
    var fw : float = 200.0f / 6 * lose;                  // 실수 형태의 fw 변수를 생성하고 실수 200 / 6 * lose 변수 값을 저장하라.
    

    GUI.DrawTexture(Rect(20, 20, 200, 30), Resources.Load("Images/progressBack")); // GUI 객체의 DrawTexture을 (20,20)위치의 (200,30)크기로 배치하라. 리소스 객체의 불러오기 주소은 Images/progressBack로 하여라.

    GUI.DrawTexture(Rect(20 + fw, 20, 200 - fw, 30), Resources.Load("Images/progressBar")); // GUI 객체의 DrawTexture를  (20 + fw,20)위치의 (200 - fw,30)크기로 배치하라. 리소스 객체의 불러오기 주소은 Images/progressBar로 하여라.

}

function EnemyEnergy(hit : int) { //EnemyEnergy의 속성이 int 형태의 객체인 함수을 다음과 같이 정의 하여라

    var w = Screen.width;                                // 화면의 폭 값을 변수 w에 생성하고 저장하라
    
    var fw2 : float = 200.0f / 6 * hit;                  // 실수 형태의 fw 변수를 생성하고 실수 200 / 6 * hit 변수 값을 저장하라.
    
   
    GUI.DrawTexture(Rect(w-220, 20, 200, 30), Resources.Load("Images/progressBack")); // GUI 객체의 DrawTexture를 불러와 (20,20)위치의 (200,30)크기로 배치하라. 리소스 객체의 불러오기 주소은 Images/progressBack로 하여라.
    
    GUI.DrawTexture(Rect(w-220 + fw2, 20, 200 - fw2, 30), Resources.Load("Images/progressBar2")); // GUI 객체의 DrawTexture를 불러와 (20 + fw,20)위치의 (200 - fw,30)크기로 배치하라. 리소스 객체의 불러오기 주소은 Images/progressBar2로 하여라.

}
